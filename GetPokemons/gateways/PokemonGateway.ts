import axios, { AxiosResponse } from "axios";

import { inject, injectable } from "inversify";
import { IPokemon } from "../services/IPokemon";
import { IPokemonGateway } from "./IPokemonGateway";
import { ILogger } from "../../commonServices/iLogger";
import { COMMON_TYPES } from "../../ioc/commonTypes";

@injectable()
export class PokemonGateway implements IPokemonGateway {

    @inject(COMMON_TYPES.ILogger)
    private readonly _logger: ILogger;

    public async fetchPokemonsByType(type: string): Promise<IPokemon[]> {
        try {
            const res: AxiosResponse = await axios.get(`https://pokeapi.co/api/v2/type/${type}`);
    
            const pokemons: IPokemon[] = res.data.pokemon.map((pokemon): IPokemon => {
                const id: number = this.extractIdFromUrl(pokemon.pokemon.url);
                const name: string = pokemon.pokemon.name;
                return { id, name };
            });
    
            return pokemons;
        } catch (err) {
            this._logger.error(err);
            throw new Error(err.response.status);
        }
    }

    private extractIdFromUrl(url: string): number {
        const splitedUrl: string[] = url.split("/");
        const index: number = splitedUrl.length - 2;
        const id: number = parseInt(splitedUrl[index], 10);
        return id;
    }
}
