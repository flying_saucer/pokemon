import { IPokemon } from "../services/IPokemon";

export interface IPokemonGateway {
    fetchPokemonsByType(type: string): Promise<IPokemon[]>;
}
