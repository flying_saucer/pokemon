import _ from "lodash";

import { inject, injectable } from "inversify";
import { IGetPokemonService } from "./IGetPokemonService";
import { COMMON_TYPES } from "../../ioc/commonTypes";
import { ILogger } from "../../commonServices/iLogger";
import { IPokemonGateway } from "../gateways/IPokemonGateway";
import { IMessage } from "./IMessage";
import { IPokemon } from "./IPokemon";

@injectable()
export class GetPokemonService implements IGetPokemonService<IMessage> {

    @inject(COMMON_TYPES.ILogger)
    private readonly _logger: ILogger;

    @inject(COMMON_TYPES.IPokemonGateway)
    private readonly _pokemonGateway: IPokemonGateway;

    public async processMessageAsync({ ids, type }: IMessage): Promise<any> {
        const pokemons: IPokemon[] = await this._pokemonGateway.fetchPokemonsByType(type);

        const wantedPokemons: IPokemon[] = this.filterPokemonsById(pokemons, ids);

        return wantedPokemons;
    }

    private filterPokemonsById(pokemons: IPokemon[], ids: number[]): IPokemon[] {
        const wantedPokemons: IPokemon[] = _.intersectionWith(pokemons, ids, (pokemon, wantedPokemonId) => {
            return pokemon.id === wantedPokemonId;
        });
        return wantedPokemons;
    }
}
