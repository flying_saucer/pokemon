import { IPokemon } from "./IPokemon";

export interface IGetPokemonService<T> {
    processMessageAsync(message: T): Promise<IPokemon[]>;
}
