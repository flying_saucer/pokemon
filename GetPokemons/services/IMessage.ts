export interface IMessage {
    ids: number[];
    type: string;
}
