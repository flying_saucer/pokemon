import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import getContainer from "../ioc/inversify.config";
import { COMMON_TYPES } from "../ioc/commonTypes";
import { Logger } from "../commonServices/logger";
import { ILogger } from "../commonServices/iLogger";
import { Container } from "inversify";
import { IPokemon } from "./services/IPokemon";
import { IMessage } from "./services/IMessage";
import { IGetPokemonService } from "./services/IGetPokemonService";

interface IResponse {
    pokemons: string[];
}

const getPokemons: AzureFunction = async (ctx: Context, req: HttpRequest): Promise<any> => {
    const container: Container = getContainer();
    const logger: Logger = container.get<ILogger>(COMMON_TYPES.ILogger) as Logger;
    logger.init(ctx, "1");

    const getPokemonService: IGetPokemonService<IMessage> =
        container.get<IGetPokemonService<IMessage>>(COMMON_TYPES.IGetPokemonService);

    try {
        const msg: IMessage = extractRequestData(req);

        const pokemons: IPokemon[] = await getPokemonService.processMessageAsync(msg);

        const response: IResponse = serializeResponse(pokemons);
    
        ctx.res = {
            body: response,
            status: 200,
            headers: { "Content-Type": "application/json" },
        };

        return ctx.res;
    } catch (err) {
        ctx.res = {
            status: err.message,
            headers: { "Content-Type": "application/json" },
        };
        
        return ctx.res;
    }

    function extractRequestData(request: HttpRequest): IMessage {
        try {
            const ids: number[] = request.query.id.split(",").map((id) => parseInt(id, 10));
            const type: string = request.query.type;
        
            if (ids.some((id) => !Number(id))) {
                throw new Error("Ids should be numbers");
            }
            if (!request.query.type) {
                throw new Error("Missing pokemon type");
            }

            return { ids, type };
        } catch (err) {
            logger.error(err);
            throw new Error("400");
        }
    }

    function serializeResponse(pokemons: IPokemon[]): IResponse {
        return {
            pokemons: pokemons.map((pokemon) => pokemon.name),
        };
    }
};

export default getPokemons;
