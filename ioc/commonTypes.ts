export const COMMON_TYPES: any = {
    ILogger: Symbol.for("ILogger"),
    IFunctionService: Symbol.for("IFunctionService"),
    IGetPokemonService: Symbol.for("IGetPokemonService"),
    IPokemonGateway: Symbol.for("IPokemonGateway"),
};
