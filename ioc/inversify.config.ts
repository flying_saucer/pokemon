import "reflect-metadata";
import {Container} from "inversify";
import {COMMON_TYPES} from "./commonTypes";

import {Logger} from "../commonServices/logger";
import {ILogger} from "../commonServices/iLogger";
import { IFunctionService } from "../HttpTrigger/services/IFunctionService";
import { FunctionService } from "../HttpTrigger/services/FunctionService";
import { IGetPokemonService } from "../GetPokemons/services/IGetPokemonService";
import { GetPokemonService } from "../GetPokemons/services/GetPokemonService";
import { IMessage } from "../GetPokemons/services/IMessage";
import { IPokemonGateway } from "../GetPokemons/gateways/IPokemonGateway";
import { PokemonGateway } from "../GetPokemons/gateways/PokemonGateway";

const getContainer: (() => Container) = (): Container => {
    const container: Container = new Container();
    
    container
        .bind<ILogger>(COMMON_TYPES.ILogger)
        .to(Logger)
        .inSingletonScope();
    
    container
        .bind<IFunctionService<any>>(COMMON_TYPES.IFunctionService)
        .to(FunctionService);
        
    container
        .bind<IGetPokemonService<IMessage>>(COMMON_TYPES.IGetPokemonService)
        .to(GetPokemonService);
    
    container
        .bind<IPokemonGateway>(COMMON_TYPES.IPokemonGateway)
        .to(PokemonGateway);
    
    return container;
};

export default getContainer;
